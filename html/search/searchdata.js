var indexSectionsWithContent =
{
  0: "_abcdefgiklmnoprstuw",
  1: "t",
  2: "acdegkmoptw",
  3: "_acdefgkmnopstuw",
  4: "_cegklmnoprsuw",
  5: "efgw",
  6: "acgrw",
  7: "_bcdefgikmtu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros"
};

