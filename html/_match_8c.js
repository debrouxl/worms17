var _match_8c =
[
    [ "Match_allowWormSelection", "_match_8c.html#ad37c121d0a3f223da84ed8bbf70b86f5", null ],
    [ "Match_artilleryMode", "_match_8c.html#a4afc4870f9b1de0318757d2026323e24", null ],
    [ "Match_defaultWeapons", "_match_8c.html#ad8a6ddae5a00f0735522f89cea9f08ff", null ],
    [ "Match_dudMines", "_match_8c.html#a77c7a968b875d8001fef638ceef5a4e2", null ],
    [ "Match_gravestones", "_match_8c.html#aab72ee6c3dcb3f1dda1f670361fd47a3", null ],
    [ "Match_healthCratesEnabled", "_match_8c.html#a89dcd484e7290995cf8a8b033613c643", null ],
    [ "Match_mineFuseLength", "_match_8c.html#ad69b2f4ce234a6758d0f7a7e585fa9a4", null ],
    [ "Match_minesEnabled", "_match_8c.html#a5a4a691008b61bde6a36dc38d581ee20", null ],
    [ "Match_oilDrumsEnabled", "_match_8c.html#af100265ab98f5d2242f450a84d0cc3c6", null ],
    [ "Match_toolCratesEnabled", "_match_8c.html#aeb79a866b304dbe61fd80f2fe51543a0", null ],
    [ "Match_turnTime", "_match_8c.html#a447f846c0776c119d70e70ef2ae06507", null ],
    [ "Match_weaponCratesEnabled", "_match_8c.html#af8834ee6778894144b53d2cc4def0315", null ],
    [ "Match_wormCount", "_match_8c.html#a9daedd898559f80c1b5cedb595686d61", null ],
    [ "Match_wormStartHealth", "_match_8c.html#aede24a1829cf364feb9eff874440ec56", null ]
];