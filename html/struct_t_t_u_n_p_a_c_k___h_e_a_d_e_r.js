var struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r =
[
    [ "csize_hi", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#af9ec8fb61fecad95ada5d776f5dd293b", null ],
    [ "csize_lo", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a43d1e17e67ad4e67cd2f0232eb411983", null ],
    [ "esc1", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a5f9f54a558ac7796aa898053cd2e8fba", null ],
    [ "esc2", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#ae7e6f939a1615e80134f208fb4bb4867", null ],
    [ "extralz", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#aac6b25f8203b782b4791db555ad8ae3c", null ],
    [ "gamma1", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#aee59d7942ac36e8b16a37adaf45f8498", null ],
    [ "gamma2", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#aab1f4e6e2e86b9113d4d262b92138924", null ],
    [ "magic1", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a971538b9ba2e39c7d29d8abb42be5572", null ],
    [ "magic2", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a90d5bf68427d64391ba3b6fc0fa12335", null ],
    [ "notused1", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a3ff4626eba86d9a75e115b837ebbac22", null ],
    [ "notused2", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#ae017784ab1f725fb58260d04da6c9b3d", null ],
    [ "notused3", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#af9ab33ecdf8d74f042adda8bef0f1fe0", null ],
    [ "notused4", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a7238fa44cb167d7fa59d8b25956d4828", null ],
    [ "osize_hi", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#abde62536ad356215e932ff6117814b8b", null ],
    [ "osize_lo", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a4da9678e5f16e95bb6cd9d6e126562b6", null ],
    [ "rleentries", "struct_t_t_u_n_p_a_c_k___h_e_a_d_e_r.html#a6722dd60d6cbf962a83ff5cd331b4c8d", null ]
];