// C Source File
// Created 11/12/2017; 4:26:32 PM

#include "../Headers/System/Main.h"

/*
	Game
	----
	
	This file defines the various modes and global settings for the Game.
	
	Including the Game_update() function which updates Game Logic for everything in the Main loop

	Game modes:

	0 - Worm Select Mode
			In this mode, either an spinning choose-arrow or a downward-pointing arrow appears above
			the currently selected worm. This is the beginning of a turn, and the user can select the
			worm they wish to play with, if worm selection is enabled.
			
			A grace-period timer counts down at the beginning of this mode, before the real turn-timer starts.
			
			If a worm is selected before the grace timer finishes, the turn-timer starts immediately.
			
	1 - Turn Mode
			In this mode, the main-turn timer is counting down, if it expires the turn is over.
			
			During this mode, the user can put the game into alternate modes, such as cursor, weapon selecet, etc.
			
			During this mode, weapons can be active.
			
	2 - Weapon Select Mode
			If it's a worms turn mode, and the keyWeaponsMenu is pushed, it will show a weapons menu instead of the game.
			While the weapons menu is open, the timer is still decreasing, and physics are still happening, but
			the scene is not drawn.
			
	3 - Pause Mode
			A pause menu is drawn.
			
			Pause menu DOES stop the clock / physics, and clears the screen until unpaused.
			
			The pause menu allows the user to quit the game as well.
			
	4 - Cursor Mode
			Certain weapons require the user to move a cursor around, such as air-strike, teleport, concrete donkey, etc.
			
			This mode still renders everything and keeps time/physics running.
			
			The user can press 2ND to place a cursor, ending this mode. Depending on the weapon selected, ending
			this mode will either activate the weapon and move to Turn End mode or, if it's a weapon that requires further
			action on the users part, such as a homing missle, return to Turn Mode.
			
			If the user presses ESCAPE during this mode, it will return to Turn Mode
			
	5 - Turn End Mode
			Turn end mode is avtivated by a number of conditions:
				- A worm fires a single shot-weapon
				- A worm uses a weapon such as air-strike or something else
				- A worm fires the last shot in a multi-shot weapon
				- The worm in control is hurt during it's turn (fall damage, mines, etc)
				- The turn timer runes out
				
			Depending on the way the turn was ended, there may-or-may-not be a "retreat" timer set,
			allowing the worm to continue to move, but not select or activate weapons. If the worm
			is hurt during retreat, the retreat timer is cleared and the worm can no longer move.
			
			Turn end mode has a number of complicated conditions for it to exit, which should all be automatic,
			no input from the user.
			
			In order for turn-end to exit, the following conditions must be met:
				- No active weapons objects
				- No active explosions
				- No mines counting down
				- Retreat timer at zero
				- No moving / falling / sliding worms
				- Everything should be "at-rest", meaning no falling / sliding objects anywhere
				- No active fire particles
				
			Once all the above conditions are met, Turn End mode will transition into Death Mode
			
		6 - Death Mode
				After a turn is ended, any non-drown worms with 0 health will take turns exploding into
				their gravestones, from left-most to right-most.
				
				Because worm-death explosions can kill other worms, move them, drown them, etc this mode
				also doesn't end until everything is settled:
				- No active mines
				- No moving / falling / sliding worms or objects
				- No active explosions (worm deaths could explode nearby crates or barrols)
				- No active fire particles
				
				After all the worms have exploded and things have "settled" we can continue to Crate Spawn mode
			
		7 - After Turn Mode
				After a turn is comlpete, but before the next turn, occasionally crates can spawn.
				
				A random number will be picked, and if within threshold, a random crate will spawn somewhere on the
				map, with a parachute attached.
				
				Before this mode can exit, the crate must land on the ground, and "settle". If the user presses
				the keyAction, the parachut will cancel and regular gravity will apply.
				
				This mode is also where the water will rise if sudden death is enabled
				
		8 - Game Over
				When one team is completely elimated, or the game ends in a draw, it automatically goes to Game Over mode.
				
				If there is a winner, it will be announced and their remaining worms shall dance.
				
				If there is a draw, it will be announced.
				
				After the user confirms the annoucement with any key, the game will exit.
*/

// current game mode!
char Game_mode = gameMode_WormSelect;

// used when certain game modes require the ability to "go back" to the previous mode
char Game_previousMode = gameMode_WormSelect;

// the main timer for a turn
short Game_timer = 0;

// the retreat timer, if any
short Game_retreatTimer = 5;

// the grace-timer for WormSelect mode
short Game_graceTimer = 5;

// sudden death timer - 10 minutes before sudden death begins
long Game_suddenDeathTimer = (long)((long)((long)60*(long)TIME_MULTIPLIER) * (long)10);

// water level for sudden death:
char Game_waterLevel = 0;

// the current team, and the current selected worm on each team:
char Game_currentTeam = 1;
char Game_currentWormUp[2] = {0, 0};

// the current cursor position, facing direction, xMark spot position and settings
short Game_cursorX = 0;
short Game_cursorY = 0;
char Game_cursirDir = 0;
short Game_xMarkSpotX = 0;
short Game_xMarkSpotY = 0;
char Game_xMarkPlaced = FALSE;
char Game_xMarkAllowedOverLand = TRUE;
char Game_cursorEndTurn = FALSE;
	
/* 
	For reference, Game modes:
	
	gameMode_WormSelect
	gameMode_Turn
	gameMode_WeaponSelect
	gameMode_Pause
	gameMode_Cursor
	gameMode_TurnEnd
	gameMode_Death
	gameMode_AfterTurn
*/

// function prototypes for our local game-mode logic methods!
/**
	Called on the first-frame when the Games state machine is set to WormSelect mode.
*/
void WormSelect_enter();
/**
	Called every frame that the Games state machine is in WormSelect mode.
*/
void WormSelect_update();
/**
	Called on the first-frame when the Games state machine leaves WormSelect mode.
*/
void WormSelect_exit();
/**
	Called on the first-frame when the Games state machine is set to Turn mode.
*/
void Turn_enter();
/**
	Called every frame that the Games state machine is in Turn mode.
*/
void Turn_update();
/**
	Called on the first-frame when the Games state machine leaves Turn mode.
*/
void Turn_exit();
/**
	Called on the first-frame when the Games state machine is set to WeaponSelect mode.
*/
void WeaponSelect_enter();
/**
	Called every frame that the Games state machine is in WeaponSelect mode.
*/
void WeaponSelect_update();
/**
	Called on the first-frame when the Games state machine leaves WeaponSelect mode.
*/
void WeaponSelect_exit();
/**
	Called on the first-frame when the Games state machine is set to Pause mode.
*/
void Pause_enter();
/**
	Called every frame that the Games state machine is in Pause mode.
*/
void Pause_update();
/**
	Called on the first-frame when the Games state machine leaves Pause mode.
*/
void Pause_exit();
/**
	Called on the first-frame when the Games state machine is set to Cursor mode.
*/
void Cursor_enter();
/**
	Called every frame that the Games state machine is in Cursor mode.
*/
void Cursor_update();
/**
	Called on the first-frame when the Games state machine leaves Cursor mode.
*/
void Cursor_exit();
/**
	Called on the first-frame when the Games state machine is set to TurnEnd mode.
*/
void TurnEnd_enter();
/**
	Called every frame that the Games state machine is in TurnEnd mode.
*/
void TurnEnd_update();
/**
	Called on the first-frame when the Games state machine leaves TurnEnd mode.
*/
void TurnEnd_exit();
/**
	Called on the first-frame when the Games state machine is set to Death mode.
*/
void Death_enter();
/**
	Called every frame that the Games state machine is in Death mode.
*/
void Death_update();
/**
	Called on the first-frame when the Games state machine leaves Death mode.
*/
void Death_exit();
/**
	Called on the first-frame when the Games state machine is set to AfterTurn mode.
*/
void AfterTurn_enter();
/**
	Called every frame that the Games state machine is in AfterTurn mode.
*/
void AfterTurn_update();
/**
	Called on the first-frame when the Games state machine leaves AfterTurn mode.
*/
void AfterTurn_exit();
/**
	Called on the first-frame when the Games state machine is set to GameOver mode.
*/
void GameOver_enter();
/**
	Called every frame that the Games state machine is in GameOver mode.
*/
void GameOver_update();
/**
	Called on the first-frame when the Games state machine leaves GameOver mode.
*/
void GameOver_exit();



// --------------------------------------------------------------------------------------------------------------------------------------



/*
	This is the MAIN update for the entire game! it all goes down here!

  Note that this is programmed kinda like a state-machine,
  and the Game_mode variable should only be changed with our Game_changeMode() function,
  never directly.
  
  This way we can have enter and exit methods for our modes! nifty!
*/
void Game_update()
{
	// before we do anything else, we should update the states of the keys
	Keys_update();
	
	// always update the camera, even when we arent rendering the game (easier this way)
	Camera_update();
	
	// depending on the current game mode, different logic will be present for each mode
	switch(Game_mode)
	{
		case gameMode_WormSelect:
			WormSelect_update();
			break;
		case gameMode_Turn:
			Turn_update();
			break;
		case gameMode_WeaponSelect:
			WeaponSelect_update();
			break;
		case gameMode_Pause:
			Pause_update();
			break;
		case gameMode_Cursor:
			Cursor_update();
			break;
		case gameMode_TurnEnd:
			TurnEnd_update();
			break;
		case gameMode_Death:
			Death_update();
			break;
		case gameMode_AfterTurn:
			AfterTurn_update();
			break;
		case gameMode_GameOver:
			GameOver_update();
			break;
	}	
}

// changes the game mode!
void Game_changeMode(char newMode)
{
	// call the exit method for the current mode
	switch(Game_mode)
	{
		case gameMode_WormSelect:
			WormSelect_exit();
			break;
		case gameMode_Turn:
			Turn_exit();
			break;
		case gameMode_WeaponSelect:
			WeaponSelect_exit();
			break;
		case gameMode_Pause:
			Pause_exit();
			break;
		case gameMode_Cursor:
			Cursor_exit();
			break;
		case gameMode_TurnEnd:
			TurnEnd_exit();
			break;
		case gameMode_Death:
			Death_exit();
			break;
		case gameMode_AfterTurn:
			AfterTurn_exit();
			break;
		case gameMode_GameOver:
			GameOver_exit();
			break;
	}	
	
	// save our current mode as our previous mode
	Game_previousMode = Game_mode;
	
	// change the game mode officially
	Game_mode = newMode;
	
	// call the enter method for the new mode!
	switch(Game_mode)
	{
		case gameMode_WormSelect:
			WormSelect_enter();
			break;
		case gameMode_Turn:
			Turn_enter();
			break;
		case gameMode_WeaponSelect:
			WeaponSelect_enter();
			break;
		case gameMode_Pause:
			Pause_enter();
			break;
		case gameMode_Cursor:
			Cursor_enter();
			break;
		case gameMode_TurnEnd:
			TurnEnd_enter();
			break;
		case gameMode_Death:
			Death_enter();
			break;
		case gameMode_AfterTurn:
			AfterTurn_enter();
			break;
		case gameMode_GameOver:
			GameOver_enter();
			break;
	}	
}



/* ----------------------------------------------------------------------------------------
	 GLOBAL GAME METHODS +++ GLOBAL GAME METHODS +++ GLOBAL GAME METHODS +++ GLOBAL GAME METH
   ---------------------------------------------------------------------------------------- */
/**
 * Updates all the Games main timers each frame, should be called every frame.
 *
 * Updates the sudden death timer. (Frames till sudden death mode)
 * Updates the grace timer. (Frames till worm-select grace timer is up)
 * Updates the games main turn timer. (Frames till a turn is up)
*/
void gameTimers();

/**
 * Handles all the main updates for the Game mode, should be called every frame.
 *
 * That is, weather it's a worm-select mode, turn mode, or whatever,
 * the state of the game needs to be updating everyframe.
*/
void gameUpdates();

/**
 * Starts the sudden death mode, by setting all worms to 1 HP
*/
void startSuddenDeath();

// decrement's all Game Timers
void gameTimers()
{
	// always decreates the sudden death timer
	// Game_suddenDeathTimer--;
	
	// note: technically suddenDeathTimer will go negative, but I doubt anyone will play
	// long enough to get a negative overflow without drowning, so no need to check for negatives
	// the benefit of this, is that when it's 0 for just one frame, we can init sudden death
	if(Game_suddenDeathTimer==0)
		startSuddenDeath();
		
	// decrease whichever timer is active
	if(Game_graceTimer>0)
		Game_graceTimer--;
	else
		Game_timer--;
		
	// if we hit zero, end the turn!
	// note the states after turn will keep decremending the timers, so this will go negative
	// we only want to end the turn on the frame that it hit 0
	if(Game_timer==0)
		Game_changeMode(gameMode_TurnEnd);
}


// this handles all the updates for the Game mode.
// this is not called during the pause menu, but everywhere else, mostly yes
void gameUpdates()
{
	// decrease game timers
	gameTimers();
	
	// update OilDrums, Crates, Mines, Weapons
	if(OilDrum_active)
		OilDrums_update();

	if(Crate_active>0)
		Crates_update();
	
	if(Mine_active>0)
		Mines_update();
		
	if(Weapon_active>0)
		Weapons_update();
	
	// update explosions
	// NOTE: this comes last because after an explosion has had its first frame
	// it disables that bit... gotta make sure everyone else on this frame has a
	// chance to see it
	Explosion_update();
}


// sets all the worm's health to 1
// we don't need a separate variable to know that Sudden Death is on, we can always test against
// the timer variable being less than 0
void startSuddenDeath()
{
	// we don't even need to test for active worms, just make 'em all 1hp
	short i=0;
	for(i=0; i<16; i++)
		Worm_health[i]=1;
}

/*
	Due to TIGCC's weird path rules, these files are stored in the Game/States folder on DISK
	but in TIGCC IDE they are under Header Files/States
	
	The only way I was able to include them was to move them under Headers...
	
	At least in Headers they wont compile on their own, since we just want them included in this file.
	
	Anyway, below, each of the states for the state machine is defined, and any methods or variables they need
	will be defined in their file.
	
	Each state has a State_enter, State_update, and State_exit method. Yay, state-machines!
*/
#include "..\States\WormSelect.c"
#include "..\States\Turn.c"
#include "..\States\WeaponSelect.c"
#include "..\States\Pause.c"
#include "..\States\Cursor.c"
#include "..\States\TurnEnd.c"
#include "..\States\Death.c"
#include "..\States\AfterTurn.c"
#include "..\States\GameOver.c"